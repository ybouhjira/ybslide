/*
 * ybSlide version 0.1
 * Youssef Bouhjira
 */

jQuery.fn.ybSlide = function(opt){
    var slide = this ;
    var items = this.find('li');
    var imgs = this.find('img');
    // DEFAULT OPTION ----------------------------------------------------------
    opt = opt || {} ;
    opt.width= opt.width || "100%",
    opt.height= opt.height || '';
    opt.effect = opt.effect || 'slideIn' ;
    opt.animationTime = opt.animationTime || 3000 ; 
    opt.animationDelay = opt.animationDelay || 1000 ;

    // SETTING UP WIDTH AND HEIGHT ---------------------------------------------
    this.css('width', opt.width);
    // resizing images to 100% of the slideShow
    var resizeImagesToSlide = function (){
        imgs.each(function(){
            $(this).width(slide.width());
        });
    };
    resizeImagesToSlide();
    window.addEventListener('resize',resizeImagesToSlide);

    // ANIMATING
    var count = 1;
    var startAnimation = function(){
        switch(opt.effect){
        case 'slideIn':
                
                $(items[count%items.length])
                .css('z-index',count)
                .animate({
                    left: 0
                },opt.animationTime,
                function(){
                    $(items[(count-1)%items.length]).css('left',"100%");
                    count++;
                    setTimeout(startAnimation,opt.animationDelay);
                });
            break;
        }
    }
    this.height($(imgs[0]).height());
    $(items[0]).css('left',0);
    setTimeout(startAnimation,opt.animationDelay);
}